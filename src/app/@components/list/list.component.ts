import { Component, ElementRef, Input, OnInit, Output, ViewChild, EventEmitter, QueryList } from '@angular/core';
import * as $ from 'jquery';

@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.scss']
})

export class ListComponent implements OnInit {
  @Input('movementsList') movementsList: any;
  @Output('getMoreInformation') getMoreInformation: EventEmitter<any> = new EventEmitter();
  @ViewChild('selectElem') el: ElementRef;
  constructor(public host: ElementRef) { }

  ngOnInit(): void { }

  ngAfterViewInit() {
    $(this.el.nativeElement).scroll(() => {
      this.handleLast(this.el)
    })

  }

  handleLast(el: ElementRef) {
    if (el.nativeElement.scrollHeight - el.nativeElement.scrollTop == 250) {
      setTimeout(() => {
        this.getMoreInformation.emit({
          offset: this.movementsList.length,
          max: this.movementsList.length + 10
        })
      }, 150)
    }
  }

  ngOnDestroy() {
    $(this.el.nativeElement).off('scroll');
  }

}

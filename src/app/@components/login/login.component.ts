import { Component, OnInit } from '@angular/core';
import { AbstractControl, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { LoginService } from 'src/app/@services/login/login-service.service';
import { UserService } from 'src/app/@services/user/user.service';
import { CommonConfiguration } from 'src/app/@shared/enums/common.enums';
import { SessionStorageHelper } from 'src/app/@shared/helpers/sessionstorage.helper';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  validateForm!: FormGroup;
  public userFind = false
  submitForm(): void {
    for (const i in this.validateForm.controls) {
      if (this.validateForm.controls.hasOwnProperty(i)) {
        this.validateForm.controls[i].markAsDirty();
        this.validateForm.controls[i].updateValueAndValidity();
      }
    }
  }

  constructor(
    private fb: FormBuilder,
    private loginService: LoginService,
    private userService: UserService,
    private readonly router: Router) { }

  ngOnInit(): void {

    this.validateForm = this.fb.group({
      userName: [null, [Validators.required]],
      password: [null, [Validators.required]]
    });

  }

  handleAuhtUser() {
    const password = this.validateForm.get('password')?.value;
    const username = this.validateForm.get('userName')?.value;
    if (password && username) {
      this.loginService.loginAuth({
        password: password, username: username
      }).subscribe((response) => {
        SessionStorageHelper.Save(CommonConfiguration.Token, response.access_token)
        this.userService.getExtraInformationUser().subscribe((responseExtra) => {
          SessionStorageHelper.Save(CommonConfiguration.UserInformation, JSON.stringify(response))
          SessionStorageHelper.Save(CommonConfiguration.UserInformationExtra, JSON.stringify(responseExtra))
          this.router.navigate(['/movements']);
        })
      }, () => {
        this.userFind = true
        setTimeout(() => {
          this.userFind = false
        }, 1900)
      })
    } else {
      this.validateForm.get('password')?.markAsTouched()
      this.validateForm.get('userName')?.markAsTouched()
    }

  }


}

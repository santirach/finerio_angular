import { Component, OnInit } from '@angular/core';
import { MovementsService } from 'src/app/@services/movements/movements.service';
import { CommonConfiguration } from 'src/app/@shared/enums/common.enums';
import { SessionStorageHelper } from 'src/app/@shared/helpers/sessionstorage.helper';
import { ProgressSpinnerMode } from '@angular/material/progress-spinner';
import { Router } from '@angular/router';
@Component({
  selector: 'app-movements',
  templateUrl: './movements.component.html',
  styleUrls: ['./movements.component.scss']
})
export class MovementsComponent implements OnInit {
  public movementsList: Array<any> = []

  constructor(
    private movementsService: MovementsService,
    private readonly router: Router
  ) { }
  public userInformacion: any;
  public userInformacionExtra: any;

  ngOnInit(): void {
    this.userInformacion = JSON.parse(SessionStorageHelper.Get(CommonConfiguration.UserInformation))
    this.userInformacionExtra = JSON.parse(SessionStorageHelper.Get(CommonConfiguration.UserInformationExtra))
    this.getMovements()

  }

  getMovements(offset = 0, max = 10) {
    const movementRequest = {
      idUser: this.userInformacionExtra.id,
      offset: offset, max: max
    }
    this.movementsService.getMovements(movementRequest).subscribe((responseMovement) => {
      this.movementsList = [...this.movementsList, ...responseMovement.data]
    })
  }

  getMoreInformation(event: { offset: number, max: number }) {

    this.getMovements(event.offset, event.max)
  }


  sessionFinish() {
    SessionStorageHelper.RemoveAll()
    this.router.navigate(['/login']);
  }

}

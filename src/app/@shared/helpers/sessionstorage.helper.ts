import { CommonConfiguration } from '../enums/common.enums';
/* import { CriptografiaHelper } from './criptografia.helper'; */

export class SessionStorageHelper {

  static Save(key: string, value: string): void {
    sessionStorage.setItem(key, value);
  }

  static Get(key: string): string {
    let result = '';
    let value = sessionStorage.getItem(key);
    if (value !== null && value !== undefined) {

      result = value;

    }
    return result;
  }

  static Remove(key: string): void {
    sessionStorage.removeItem(key);
  }

  static RemoveAll(): void {
    sessionStorage.removeItem(CommonConfiguration.Token);
    sessionStorage.removeItem(CommonConfiguration.UserInformation);
    sessionStorage.removeItem(CommonConfiguration.UserInformationExtra);

  }
}

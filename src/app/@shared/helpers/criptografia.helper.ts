
import * as CryptoJS from 'crypto-js';
import { environment } from 'src/app/@environments/environment';
export class CriptografiaHelper {

  /**
   * @author Elkin Vasquez Isenia
   * @sprint 1
   * @param value
   * @param secret
   * @returns
   */
  static Encrypt(value: string, secret: string) {
    var keySize = 256;
    var salt = CryptoJS.lib.WordArray.random(16);
    var key = CryptoJS.PBKDF2(secret, salt, {
      keySize: keySize / 32,
      iterations: 100,
    });

    var iv = CryptoJS.lib.WordArray.random(128 / 8);

    var encrypted = CryptoJS.AES.encrypt(value, key, {
      iv: iv,
      padding: CryptoJS.pad.Pkcs7,
      mode: CryptoJS.mode.CBC,
    });

    var result = CryptoJS.enc.Base64.stringify(
      salt.concat(iv).concat(encrypted.ciphertext)
    );

    return result;
  }

  /**
   * @author Elkin Vasquez Isenia
   * @sprint 1
   * @param ciphertextB64
   * @param secret
   * @returns
   */
  static Decrypt(ciphertextB64: string, secret: string) {
    var key = CryptoJS.enc.Utf8.parse(secret);
    var iv = CryptoJS.lib.WordArray.create([0x00, 0x00, 0x00, 0x00]);

    var decrypted = CryptoJS.AES.decrypt(ciphertextB64, key, { iv: iv });
    return decrypted.toString(CryptoJS.enc.Utf8);
  }

  /**
 * @author Elkin Vasquez Isenia
 * @sprint 2
 * @param value
 * @returns
 */
  static EncryptAes(value: string) {
    var ciphertext = CryptoJS.AES.encrypt(value, environment.crypto.secret).toString();
    return ciphertext;
  }

  /**
   * @author Elkin Vasquez Isenia
   * @sprint 2
   * @param value
   * @returns
   */
  static DecryptAes(value: string) {
    var bytes = CryptoJS.AES.decrypt(value, environment.crypto.secret);
    return bytes.toString(CryptoJS.enc.Utf8);
  }
}

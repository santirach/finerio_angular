export enum CommonConfiguration {
  AuthenticationScheme = 'Bearer',
  Token = 'TokenId',
  UserInformation = 'UserInformation',
  UserInformationExtra = 'UserInformationExtra'

}

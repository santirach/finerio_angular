import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot, UrlTree } from '@angular/router';
import { Observable } from 'rxjs';
import { CommonConfiguration } from 'src/app/@shared/enums/common.enums';
import { SessionStorageHelper } from 'src/app/@shared/helpers/sessionstorage.helper';

@Injectable({
  providedIn: 'root'
})
export class AuthenticationGuard implements CanActivate {
  constructor(
    private readonly router: Router,
  ) { }

  public isAuthenticated(): boolean {
    const token = SessionStorageHelper.Get(CommonConfiguration.Token)

    if (token !== undefined && token !== null && token.length > 0) {
      return true;

    } else {
      return false;
    }
  }

  canActivate(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): boolean {

    if (this.isAuthenticated()) {
      return true;
    } else {
      this.router.navigate(['/login']);

      return false
    }

  }

}

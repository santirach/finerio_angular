import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { LoginComponent } from './@components/login/login.component';
import { MovementsComponent } from './@components/movements/movements.component';
import { AuthenticationGuard } from './@core/authentication/authentication.guard';

const routes: Routes = [
  {
    path: '',
    redirectTo: '/login',
    pathMatch: 'full'
  },
  { path: 'login', component: LoginComponent },
  { path: 'movements', component: MovementsComponent, canActivate: [AuthenticationGuard] },
  { path: '**', redirectTo: '/login', pathMatch: "full" },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }

import { HttpResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { environment } from 'src/app/@environments/environment';
import { HttpService } from '../http.service';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  constructor(
    private httpService: HttpService
  ) { }

  public getExtraInformationUser(): Observable<any> {
    let response$: Observable<any>;
    response$ = this.httpService
      .Get(
        `${environment.apiHost}/me`
      )
      .pipe(
        map((response: HttpResponse<any>) => {
          return response.body;
        })
      );
    return response$;
  }
}

import { HttpResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { environment } from 'src/app/@environments/environment';
import { HttpService } from '../http.service';

@Injectable({
  providedIn: 'root'
})
export class MovementsService {

  constructor(
    private httpService: HttpService
  ) { }

  public getMovements(body: {
    idUser: any,
    offset: any,
    max: any
  }): Observable<any> {
    let response$: Observable<any>;
    response$ = this.httpService
      .Get(
        `${environment.apiHost}/users/${body.idUser}/movements?deep=true&offset=${body.offset}&max=${body.max}&includeCharges=true&includeDeposits=true&includeDuplicates=false`,
      )
      .pipe(
        map((response: HttpResponse<any>) => {
          return response.body;
        })
      );
    return response$;
  }
}
